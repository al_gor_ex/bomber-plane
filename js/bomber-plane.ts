import { bomberPlaneSpeed, bomberPlaneBombsCount } from "./config.js"

export class BomberPlane {
    public x: number
    public y: number
    public bombsCount: number
    public speedX: number

    constructor(x: number, y: number) {
        this.x = x
        this.y = y
        this.speedX = bomberPlaneSpeed
        this.bombsCount = bomberPlaneBombsCount
    }

    public move(): void {
        this.x += this.speedX
    }
}