export var warShipsCount = 3;
export var warShipWidth = 170;
export var warShipHeight = 65;
export var bomberPlaneBombsCount = 7;
export var bomberPlaneSpeed = 2.5;
export var bombAccelerationY = 0.05;
