export let warShipsCount: number = 3
export let warShipWidth = 170
export let warShipHeight = 65
export let bomberPlaneBombsCount: number = 7
export let bomberPlaneSpeed: number = 2.5
export let bombAccelerationY: number = 0.05