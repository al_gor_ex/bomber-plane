import { bomberPlaneSpeed, bomberPlaneBombsCount } from "./config.js";
var BomberPlane = (function () {
    function BomberPlane(x, y) {
        this.x = x;
        this.y = y;
        this.speedX = bomberPlaneSpeed;
        this.bombsCount = bomberPlaneBombsCount;
    }
    BomberPlane.prototype.move = function () {
        this.x += this.speedX;
    };
    return BomberPlane;
}());
export { BomberPlane };
