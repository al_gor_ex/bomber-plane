import { bomberPlaneSpeed, bombAccelerationY } from "./config.js";
var Bomb = (function () {
    function Bomb(x, y) {
        this.x = x;
        this.y = y;
        this.speedX = bomberPlaneSpeed / 1.1;
        this.speedY = 0;
    }
    Bomb.prototype.move = function () {
        this.x += this.speedX;
        this.y += this.speedY;
        this.speedY += bombAccelerationY;
    };
    return Bomb;
}());
export { Bomb };
