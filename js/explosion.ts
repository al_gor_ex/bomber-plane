export class Explosion {
    public x: number
    public y: number
    public opacity: number

    constructor(x: number, y: number) {
        this.x = x
        this.y = y
        this.opacity = 1
    }

    public fade(): void {
        this.opacity -= 0.01
    }
}