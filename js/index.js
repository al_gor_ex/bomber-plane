import { WarShip } from "./warship.js";
import { BomberPlane } from "./bomber-plane.js";
import { Bomb } from "./bomb.js";
import { Explosion } from "./explosion.js";
import { warShipsCount, bomberPlaneBombsCount, warShipWidth, warShipHeight } from "./config.js";
var isPlaying = false;
var screenW;
var screenH;
var seaToScreenRatio = 4 / 5;
var context = null;
var warShips = [];
var bomberPlane;
var flyingBombs = [];
var explosions = [];
export function onLoad() {
    var canvas = document.getElementById('game-canvas');
    canvas.width = window.innerWidth;
    canvas.height = window.innerHeight;
    context = canvas.getContext('2d');
    screenW = canvas.width;
    screenH = canvas.height;
    var btnStart = document.getElementById('btn-start');
    btnStart.onclick = function () {
        onBtnStartClick();
        btnStart.style.display = 'none';
    };
    canvas.onclick = onCanvasClick;
    loop();
}
function onBtnStartClick() {
    toggleCanvas(true);
    hideMessages();
    createWarShips(warShipsCount);
    createPlane(bomberPlaneBombsCount);
    isPlaying = true;
}
export function onCanvasClick() {
    if (bomberPlane.bombsCount > 0) {
        createFlyingBomb();
    }
}
function toggleCanvas(isVisible) {
    var canvas = document.getElementById('game-canvas');
    canvas.style.display = isVisible ? 'inline' : 'none';
}
function hideMessages() {
    var messages = [document.getElementById('win-msg'), document.getElementById('loss-msg')];
    for (var _i = 0, messages_1 = messages; _i < messages_1.length; _i++) {
        var message = messages_1[_i];
        message.style.display = 'none';
    }
}
function createWarShips(count) {
    for (var i = 0; i < count; i++) {
        var newX = randomIntFromInterval(screenW * i / (count + 1), screenW * (i + 1) / (count + 1));
        var newY = screenH * (1 - (1 - seaToScreenRatio));
        warShips.push(new WarShip(newX, newY));
    }
}
function createPlane(bombsCount) {
    bomberPlane = new BomberPlane(20, 50);
}
function createFlyingBomb() {
    flyingBombs.push(new Bomb(bomberPlane.x + 20, bomberPlane.y + 15));
    bomberPlane.bombsCount--;
}
function createExplosion(x, y) {
    explosions.push(new Explosion(x - 100, y - 100));
}
function randomIntFromInterval(min, max) {
    return Math.floor(Math.random() * (max - min + 1) + min);
}
function loop() {
    if (isPlaying) {
        movePlane();
        moveBombs();
        updateExplosions();
        checkBombsPositions();
        checkFinish();
        repaint();
    }
    window.requestAnimationFrame(loop);
}
function movePlane() {
    bomberPlane.move();
    if (bomberPlane.x >= screenW) {
        bomberPlane.x = -300;
    }
}
function moveBombs() {
    for (var _i = 0, flyingBombs_1 = flyingBombs; _i < flyingBombs_1.length; _i++) {
        var bomb = flyingBombs_1[_i];
        bomb.move();
    }
}
function checkBombsPositions() {
    for (var i = 0; i < flyingBombs.length; i++) {
        if (!(warShips.length == 0)) {
            var bomb = flyingBombs[i];
            if (bomb.y >= warShips[0].y + 20) {
                var hasHitAnyWarShip = false;
                for (var j = 0; j < warShips.length; j++) {
                    var warShip = warShips[j];
                    if (bomb.x >= warShip.x &&
                        bomb.x <= warShip.x + warShipWidth &&
                        bomb.y <= warShip.y + warShipHeight) {
                        warShips.splice(j, 1);
                        flyingBombs.splice(i, 1);
                        i--;
                        createExplosion(warShip.x, warShip.y);
                        hasHitAnyWarShip = true;
                        break;
                    }
                }
                if (!hasHitAnyWarShip) {
                    flyingBombs.splice(i, 1);
                    i--;
                }
            }
        }
    }
}
function updateExplosions() {
    for (var i = 0; i < explosions.length; i++) {
        var explosion = explosions[i];
        explosion.fade();
        if (explosion.opacity <= 0) {
            explosions.splice(i, 1);
            i--;
        }
    }
}
function checkFinish() {
    if (warShips.length == 0 && explosions.length == 0) {
        finishGame(true);
    }
    else if (warShips.length > 0 && flyingBombs.length == 0 && bomberPlane.bombsCount == 0) {
        finishGame(false);
    }
}
function finishGame(isWin) {
    isPlaying = false;
    warShips = [];
    bomberPlane = null;
    flyingBombs = [];
    explosions = [];
    toggleCanvas(false);
    var id;
    id = isWin ? 'win-msg' : 'loss-msg';
    document.getElementById(id).style.display = 'inline';
    document.getElementById('btn-start').style.display = 'inline';
}
function repaint() {
    drawBackground();
    drawBombsCounter();
    drawWarShips();
    drawPlane();
    drawBombs();
    drawExplosions();
}
function drawBackground() {
    context.fillStyle = '#5693b9';
    context.fillRect(0, screenH * seaToScreenRatio, screenW, screenH * (1 - seaToScreenRatio));
    context.fillStyle = '#cfd7e0';
    context.fillRect(0, 0, screenW, screenH * seaToScreenRatio);
}
function drawBombsCounter() {
    if (bomberPlane != null) {
        context.font = '24px serif';
        var text = void 0;
        if (bomberPlane.bombsCount > 0) {
            context.fillStyle = '#019a09';
            text = "\u041E\u0441\u0442\u0430\u043B\u043E\u0441\u044C \u0431\u043E\u043C\u0431: " + bomberPlane.bombsCount;
        }
        else {
            context.fillStyle = '#ad0000';
            text = 'Бомбы закончились';
        }
        context.fillText(text, 0.8 * screenW, 0.1 * screenH);
    }
}
function drawWarShips() {
    var img = new Image();
    img.src = warShip;
    for (var _i = 0, warShips_1 = warShips; _i < warShips_1.length; _i++) {
        var ship = warShips_1[_i];
        context.drawImage(img, ship.x, ship.y, warShipWidth, warShipHeight);
    }
}
function drawPlane() {
    if (bomberPlane != null) {
        var img = new Image();
        img.src = plane;
        context.drawImage(img, bomberPlane.x, bomberPlane.y, 100, 45);
    }
}
function drawBombs() {
    var img = new Image();
    img.src = bomb;
    for (var _i = 0, flyingBombs_2 = flyingBombs; _i < flyingBombs_2.length; _i++) {
        var flyingBomb = flyingBombs_2[_i];
        context.drawImage(img, flyingBomb.x, flyingBomb.y, 50, 20);
    }
}
function drawExplosions() {
    var img = new Image();
    img.src = explosion;
    for (var _i = 0, explosions_1 = explosions; _i < explosions_1.length; _i++) {
        var expl = explosions_1[_i];
        context.save();
        context.globalAlpha = expl.opacity;
        context.drawImage(img, expl.x, expl.y);
        context.restore();
    }
}
