import { WarShip } from "./warship.js"
import { BomberPlane } from "./bomber-plane.js"
import { Bomb } from "./bomb.js"
import { Explosion } from "./explosion.js"
import { 
    warShipsCount, 
    bomberPlaneBombsCount, 
    warShipWidth, 
    warShipHeight 
} from "./config.js"
let isPlaying = false
let screenW: number
let screenH: number
let seaToScreenRatio = 4 / 5
let context: CanvasRenderingContext2D = null
let warShips: WarShip[] = []
let bomberPlane: BomberPlane
let flyingBombs: Bomb[] = []
let explosions: Explosion[] = []


export function onLoad() {
    let canvas = document.getElementById('game-canvas') as HTMLCanvasElement
    canvas.width = window.innerWidth
    canvas.height = window.innerHeight
    context = canvas.getContext('2d')
    screenW = canvas.width
    screenH = canvas.height
    let btnStart = document.getElementById('btn-start')
    btnStart.onclick = () => {
        onBtnStartClick()
        btnStart.style.display = 'none'
    }
    canvas.onclick = onCanvasClick
    loop()

}

function onBtnStartClick() {
    toggleCanvas(true)
    hideMessages()
    createWarShips(warShipsCount)
    createPlane(bomberPlaneBombsCount)
    isPlaying = true
}

export function onCanvasClick() {
    if (bomberPlane.bombsCount > 0) {
        createFlyingBomb()
    }
}

function toggleCanvas(isVisible: boolean) {
    let canvas = document.getElementById('game-canvas') as HTMLCanvasElement
    canvas.style.display = isVisible ? 'inline' : 'none'
}

function hideMessages() {
    let messages = [document.getElementById('win-msg'), document.getElementById('loss-msg')]
    for (let message of messages) {
        message.style.display = 'none'
    }
}

function createWarShips(count: number) {
    for (let i = 0; i < count; i++) {
        // screen space is divided equally between ships' zones
        let newX = randomIntFromInterval(screenW * i / (count + 1), screenW * (i + 1) / (count + 1))
        let newY = screenH * (1 - (1 - seaToScreenRatio))
        warShips.push(new WarShip(newX, newY))
    }
}

function createPlane(bombsCount: number) {
    bomberPlane = new BomberPlane(20, 50)
}

function createFlyingBomb() {
    flyingBombs.push(new Bomb(bomberPlane.x + 20, bomberPlane.y + 15))
    bomberPlane.bombsCount--
}

function createExplosion(x: number, y: number) {
    explosions.push(new Explosion(x - 100, y - 100))
}

// min and max included
function randomIntFromInterval(min: number, max: number): number {
    return Math.floor(Math.random() * (max - min + 1) + min)
}

function loop() {
    if (isPlaying) {
        movePlane()
        moveBombs()
        updateExplosions()
        checkBombsPositions()
        checkFinish()
        repaint()
    }
    window.requestAnimationFrame(loop)
}

function movePlane() {
    bomberPlane.move()
    if (bomberPlane.x >= screenW) {
        bomberPlane.x = -300
    }
}

function moveBombs() {
    for (let bomb of flyingBombs) {
        bomb.move()
    }
}

function checkBombsPositions() {
    for (let i = 0; i < flyingBombs.length; i++) {
        if (!(warShips.length == 0)) {
            // if bomb is lower than warShips height
            let bomb = flyingBombs[i]
            if (bomb.y >= warShips[0].y + 20) {
                let hasHitAnyWarShip = false
                for (let j = 0; j < warShips.length; j++) {
                    // if bomb is between the warship x borders
                    let warShip = warShips[j]
                    if (bomb.x >= warShip.x &&
                        bomb.x <= warShip.x + warShipWidth &&
                        // and bomb is not under the ship body
                        bomb.y <= warShip.y + warShipHeight
                    ) {
                        // destroy ship and bomb
                        warShips.splice(j, 1)
                        flyingBombs.splice(i, 1)
                        // one step back because the bomb was destroyed
                        i--
                        createExplosion(warShip.x, warShip.y)
                        hasHitAnyWarShip = true
                        break
                    }
                }
                if (!hasHitAnyWarShip) {
                    // destroy the bomb
                    flyingBombs.splice(i, 1)
                    // one step back because the bomb was destroyed
                    i--
                }
            }
        }
    }
}

function updateExplosions() {
    for (let i = 0; i < explosions.length; i++) {
        let explosion = explosions[i]
        explosion.fade()
        if (explosion.opacity <= 0) {
            // remove explosion
            explosions.splice(i, 1)
            i--
        }
    }
}

function checkFinish() {
    // if all warships are destroyed and no explosions are displayed
    if (warShips.length == 0 && explosions.length == 0) {
        finishGame(true)
    } else if (warShips.length > 0 && flyingBombs.length == 0 && bomberPlane.bombsCount == 0) {
        finishGame(false)
    }
}

function finishGame(isWin: boolean) {
    isPlaying = false
    warShips = []
    bomberPlane = null
    flyingBombs = []
    explosions = []
    toggleCanvas(false)
    let id: string
    id = isWin ? 'win-msg' : 'loss-msg'
    document.getElementById(id).style.display = 'inline'
    document.getElementById('btn-start').style.display = 'inline'
}

function repaint() {
    drawBackground()
    drawBombsCounter()
    drawWarShips()
    drawPlane()
    drawBombs()
    drawExplosions()
}

function drawBackground() {
    // water
    context.fillStyle = '#5693b9'
    context.fillRect(0, screenH * seaToScreenRatio, screenW, screenH * (1 - seaToScreenRatio))
    // sky
    context.fillStyle = '#cfd7e0'
    context.fillRect(0, 0, screenW, screenH * seaToScreenRatio)
}

function drawBombsCounter() {
    if (bomberPlane != null) {
        context.font = '24px serif'
        let text: string
        if (bomberPlane.bombsCount > 0) {
            context.fillStyle = '#019a09'
            text = `Осталось бомб: ${bomberPlane.bombsCount}`
        } else {
            context.fillStyle = '#ad0000'
            text = 'Бомбы закончились'
        }
        context.fillText(text, 0.8 * screenW, 0.1 * screenH)
    }
}

function drawWarShips() {
    let img = new Image()
    // defined in images.js file
    // @ts-ignore
    img.src = warShip
    for (let ship of warShips) {
        context.drawImage(img, ship.x, ship.y, warShipWidth, warShipHeight)
    }
}

function drawPlane() {
    if (bomberPlane != null) {
        let img = new Image()
        // defined in images.js file
        // @ts-ignore
        img.src = plane
        context.drawImage(img, bomberPlane.x, bomberPlane.y, 100, 45)
    }
}

function drawBombs() {
    let img = new Image()
    // defined in images.js file
    // @ts-ignore
    img.src = bomb
    for (let flyingBomb of flyingBombs) {
        context.drawImage(img, flyingBomb.x, flyingBomb.y, 50, 20)
    }
}

function drawExplosions() {
    let img = new Image()
    // defined in images.js file
    // @ts-ignore
    img.src = explosion
    for (let expl of explosions) {
        context.save()
        context.globalAlpha = expl.opacity
        context.drawImage(img, expl.x, expl.y)
        context.restore()
    }
}
