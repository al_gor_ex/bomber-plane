var Explosion = (function () {
    function Explosion(x, y) {
        this.x = x;
        this.y = y;
        this.opacity = 1;
    }
    Explosion.prototype.fade = function () {
        this.opacity -= 0.01;
    };
    return Explosion;
}());
export { Explosion };
