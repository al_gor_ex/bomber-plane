import { bomberPlaneSpeed, bombAccelerationY } from "./config.js"

export class Bomb {
    public x: number
    public y: number
    public speedX: number
    public speedY: number

    constructor(x: number, y: number) {
        this.x = x
        this.y = y
        this.speedX = bomberPlaneSpeed / 1.1
        this.speedY = 0
    }

    public move(): void {
        this.x += this.speedX
        this.y += this.speedY
        this.speedY += bombAccelerationY
    }
}